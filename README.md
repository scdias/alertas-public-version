# Alertas (Public Version)

Gestão de Alertas de valores de criptomoedas com possibilidade de alertas por Telegram

## Getting Started

Executar python3 build.py para gerar um único ficheiro pyc

### Prerequisites

Necessário python 3.5. Recomendado executar em virtual environment para instalação automática dos packages dependentes.
