from shutil import rmtree
from zipfile import PyZipFile, ZIP_DEFLATED
from datetime import datetime
from platform import machine

APP = 'Alertas.pyc'
package = 'alertas'

now = datetime.now()

versao = open(package + '/lib_versao.py', 'w+')
versao.write("versao = '{ano:04}.{mes:02}.{dia:02} para {proc}'\n"
             .format(ano=now.year, mes=now.month, dia=now.day, proc=machine()))
versao.close()

final = PyZipFile(APP, mode='w', compression=ZIP_DEFLATED, optimize=2)
final.debug = 3

final.writepy('__main__.py')
final.writepy(package)

final.testzip()
final.close()

rmtree('__pycache__')
rmtree(package + '/__pycache__')
