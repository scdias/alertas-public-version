"""Powered by scdias@outlook.com
Main Function"""

from alertas.lib_install import update, install

try:
    from signalslot import Signal
except ImportError as e:
    print('\033[94m{}\033[0m'.format(e))
    install('signalslot')
    from signalslot import Signal

try:
    from matplotlib import __version__ as matplotlib_version
except ImportError as e:
    print('\033[94m{}\033[0m'.format(e))
    install('matplotlib')
    from matplotlib import __version__ as matplotlib_version

from threading import Thread
from time import sleep

from alertas.lib_telegram import envia_admin_msg, signal_exit_telegram
from alertas.lib_versao import versao
from alertas.tbl_log import Log
from alertas.lib_utils import ler_dados, ler_diferenca_btc, ler_volume_btc
from alertas.db_config import engine
from alertas.lib_websockets import connect_websocket

keep_running = True
signal_exit_main = Signal()
connect_websocket()


def slot_exit(**kwargs):
    global keep_running
    Log('__main__: Slot exit executed!!!').save()
    envia_admin_msg(u'\U0001F480 Terminando bot....')
    engine.dispose()
    keep_running = False


signal_exit_telegram.connect(slot_exit)
signal_exit_main.connect(slot_exit)


def timer():
    global keep_running
    while keep_running:
        thr = ''
        try:
            thr = 'ler_dados'
            Thread(target=ler_dados(), daemon=True).start()
            thr = 'ler_diferenca_btc'
            Thread(target=ler_diferenca_btc()).start()
            thr = 'ler_volume_btc'
            Thread(target=ler_volume_btc()).start()
        except KeyboardInterrupt:
            Log('\n__main__: CTRL+C: A interromper timer....').save()
            signal_exit_main.emit()
        except Exception as e_timer:
            Log('__main__: Erro a iniciar Thread {}: {}'.format(thr, e_timer)).save()
        sleep(1)


update()

if __name__ == "__main__":
    print('Loading Matplotlib version {}'.format(matplotlib_version))
    Log('__main__: Iniciando Alertas versão {}....'.format(versao)).save()
    try:
        envia_admin_msg(u'\U0001F51B Iniciado sistema de alertas.')
        t = Thread(target=timer)
        t.start()
        Log('__main__: Arranque iniciado!!!!').save()
        t.join()
        Log('__main__: Alertas finalizado corretamente.').save()
    except KeyboardInterrupt:
        Log('\n__main__: CTRL+C pressionado. Terminando....').save()
        signal_exit_main.emit()
    except SystemExit as e:
        Log('__main__: Shutdown request by bot.\n{}'.format(e)).save()
    except Exception as e:
        Log('__main__: Runtime error: {}'.format(e)).save()
