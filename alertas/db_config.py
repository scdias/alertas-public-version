"""Powered by scdias@outlook.com
Configurações da ligação a base de dados"""

from alertas.lib_install import install

try:
    from sqlalchemy import __version__
except ImportError as e:
    print('\033[94m{}\033[0m'.format(e))
    install('sqlalchemy')
    from sqlalchemy import __version__

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session

print('Loading SQLAlchemy version {}'.format(__version__))
engine = create_engine('sqlite:///alertas.db')

Base = declarative_base()

session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)  # Scoped session para ser usado em multi threading


class CRUD:

    def save(self):
        local_session = Session()
        local_session.add(self)
        local_session.commit()
        # Session.remove()

    def delete(self):
        local_session = Session()
        local_session.delete(self)
        local_session.commit()
        # Session.remove()
