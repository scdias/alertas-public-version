"""Powered by scdias@outlook.com
Biblioteca para instalação automática de pacotes"""
from subprocess import call

try:
    # noinspection PyProtectedMember
    from pip._internal.utils.misc import get_installed_distributions
except ImportError:  # pip<10
    from pip import get_installed_distributions


def install(package):
    """Instala o pacote selecionado"""
    try:
        call('pip install {} --upgrade'.format(package), shell=True)
    except Exception as e:
        print('Erro a instalar as actualizações: {}\nA tentar de novo em 30 segundos'.format(e))
        install(package)


def update():
    """Actualiza todos os pacotes existentes"""
    print('A verificar actualizações....')
    packages = [dist.project_name for dist in get_installed_distributions()]
    '''
    try:
        packages.remove('Pysher')
    except ValueError as e:
        print(e)
    '''
    try:
        call("pip install --upgrade " + ' '.join(packages), shell=True)
        print('Actualizações instaladas.')
    except KeyboardInterrupt:
        print('\nCTRL+C: A cancelar actualizações....')
    except Exception as e:
        print('Erro a executar as actualizações: {}\n.A ignorar....'.format(e))


def install_all():
    print('\033[94mA verificar requisitos\033[0m')

    pckgs = [
        'sqlalchemy',
        'python-telegram-bot',
        'signalslot',
        'pysher',
        'matplotlib',
    ]

    for pckg in pckgs:
        install(pckg)
