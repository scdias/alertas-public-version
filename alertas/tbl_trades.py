"""Powered by scdias@outlook.com
Tabela actualizada por websockects para futuro tratamento estatistico"""

from json import loads

from alertas.db_config import Base, engine, CRUD
from alertas.lib_install import install

try:
    from sqlalchemy import Column, Integer, String, REAL
except ImportError as e:
    print('\033[94m{}\033[0m'.format(e))
    install('sqlalchemy')
    from sqlalchemy import Column, Integer, String, REAL


class Trades(Base, CRUD):
    __tablename__ = 'trades'
    id = Column(Integer, primary_key=True)
    amount = Column(REAL)
    price = Column(REAL)
    type = Column(Integer)
    timestamp = Column(Integer)
    buy_order_id = Column(Integer)
    sell_order_id = Column(Integer)
    currency = Column(String)

    def __init__(self, json_str='', currency=''):
        a = loads(str(json_str))

        self.id = a['id']
        self.amount = a['amount']
        self.price = a['price']
        self.type = a['type']
        self.timestamp = a['timestamp']
        self.buy_order_id = a['buy_order_id']
        self.sell_order_id = a['sell_order_id']
        self.currency = currency

    def __str__(self):
        return '{} - {} - {} - {} - {}'.format(self.id, self.amount, self.price, self.type, self.currency)


Base.metadata.create_all(engine)
