"""Powered by scdias@outlook.com
Tabela para gestão de alertas de preços"""
from time import time

from alertas.lib_install import install
from alertas.db_config import Base, engine, CRUD

try:
    from sqlalchemy import Column, Integer, String, REAL
except ImportError as e:
    print('\033[94m{}\033[0m'.format(e))
    install('sqlalchemy')
    from sqlalchemy import Column, Integer, String, REAL


class Alertas(Base, CRUD):
    __tablename__ = 'alertas'
    id = Column(Integer, primary_key=True)
    datahora = Column(Integer)
    moeda = Column(String)
    sinal = Column(String)
    valor = Column(REAL)

    def __init__(self, moeda='', sinal='', valor=0):
        self.datahora = time()
        self.moeda = moeda
        self.sinal = sinal
        self.valor = valor

    def __str__(self):
        return '{} {} {}'.format(self.moeda, self.sinal, self.valor)


Base.metadata.create_all(engine)
