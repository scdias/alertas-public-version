"""Powered by scdias@outlook.com
Módulo para gestão de info de configuração ao programa"""

from alertas.db_config import Base, engine, CRUD
from alertas.lib_install import install

try:
    from sqlalchemy import Column, Integer, String, REAL
except ImportError as e:
    print('\033[94m{}\033[0m'.format(e))
    install('sqlalchemy')
    from sqlalchemy import Column, Integer, String, REAL


class Config(Base, CRUD):
    __tablename__ = 'config'
    parametro = Column(String, primary_key=True)
    valor = Column(String)

    def __init__(self, parametro='', valor=''):
        self.parametro = parametro
        self.valor = valor

    def __str__(self):
        return '{}: {}'.format(self.parametro, self.valor)


Base.metadata.create_all(engine)
