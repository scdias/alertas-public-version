from alertas.lib_moedas import BTC
from alertas.lib_telegram import envia_admin_msg
from alertas.tbl_stats import Stats
from alertas.db_config import Session
from alertas.tbl_log import Log


def ler_dados():
    """Regista os valores das moedas na base de dados para porsterior análise"""
    """Para correr como thread usar scoped_session"""
    # TODO: reduzido número de moedas pela limitação de requests por unidade de tempo
    # moedas = [BTC, LTC, XRP, ETH, BCH]

    moedas = [BTC, ]
    for moeda in moedas:
        local = moeda()
        try:
            if local.valor.volume:
                Stats(moeda=local.simbolo, valor=local.valor).save()
            else:
                Log('lib_utils: ler_dados: Valores inválidos').save()
        except Exception as err_ler_dados:
            Log('lib_utils: ler_dados: {}'.format(err_ler_dados)).save()


def ler_diferenca_btc():
    """Calcula a variação de diferenças do extremos de BTC"""
    local_session = Session()

    stats = local_session \
        .query(Stats) \
        .filter(Stats.moeda == 'BTC') \
        .order_by(Stats.datahora.desc()) \
        .limit(2) \
        .all()

    if len(stats) != 2:
        return

    anterior = round(stats[1].maximo - stats[1].minimo, 2)
    actual = round(stats[0].maximo - stats[0].minimo, 2)

    if anterior == actual:
        return
    sinal = ''
    if anterior > actual:
        sinal = u'\u2B07'
    if anterior < actual:
        sinal = u'\u2B06'

    envia_admin_msg('{} Diferença BTC: {}'.format(sinal, actual))

    # Session.remove()


def ler_volume_btc():
    """Alerta quando o volume 24h de BTC é inferior ao limite"""

    local_session = Session()

    stats = local_session \
        .query(Stats) \
        .filter(Stats.moeda == 'BTC') \
        .order_by(Stats.datahora.desc()) \
        .limit(2) \
        .all()

    limite = 1000  # Valor triger de mensagem

    if len(stats) != 2:
        return

    op1 = (stats[0].volume < limite) and (stats[1].volume >= limite)  # subida acima 1000
    op2 = (stats[0].volume >= limite) and (stats[1].volume < limite)  # descida abaixo de 1000

    if op1 or op2:
        envia_admin_msg(u'\U0001F4B9 Volume BTC: {}'.format(stats[0].volume))

    Session.remove()


def triger_alertas(moeda=''):
    from alertas.tbl_trades import Trades
    from alertas.tbl_alertas import Alertas

    local_session = Session()

    [(actual,), (anterior,)] = local_session \
        .query(Trades.price) \
        .order_by(Trades.timestamp.desc()) \
        .filter(Trades.currency == moeda) \
        .limit(2) \
        .all()

    if actual > anterior:
        alertas = local_session \
            .query(Alertas) \
            .filter(Alertas.moeda == moeda) \
            .filter(Alertas.sinal == '>') \
            .filter(Alertas.valor < actual) \
            .all()

    elif actual < anterior:
        alertas = local_session \
            .query(Alertas) \
            .filter(Alertas.moeda == moeda) \
            .filter(Alertas.sinal == '<') \
            .filter(Alertas.valor > actual) \
            .all()
    else:
        # Session.remove()
        return

    for alerta in alertas:
        envia_admin_msg(u'\U0001F514 ALERTA: Valor de {} {} {}'.format(alerta.moeda, alerta.sinal, alerta.valor))
        alerta.delete()  # FIXME: Possível abertura de transação dentro de outra transação???

    # Session.remove()
