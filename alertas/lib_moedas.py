"""Powered by scdias@outlook.com
Classe generica de moedas"""

from json import loads
from socket import timeout
from urllib import request

from alertas.tbl_log import Log


class Moeda:
    """Classe com os parametros a serem analizados em cada moeda"""

    def __init__(self, maximo=0, minimo=0, actual=0, volume=0):
        self.maximo = maximo
        self.minimo = minimo
        self.actual = actual
        self.volume = volume

    def __str__(self):
        return ('Actual: {}\n'
                u'M\u00E1ximo 24h: {}\n'
                u'M\u00EDnimo 24h: {}\n'
                'Volume 24h: {}'
                .format(self.actual, self.maximo, self.minimo, self.volume))


class Crypto:
    url_api = 'https://api.cryptowat.ch/markets/bitstamp/xxxeur/summary'
    nome = 'XXX'
    simbolo = u'\u20bf'
    valor = Moeda()

    def __init__(self):
        moeda = self.__ler_api()
        if moeda is None:
            Log('Erro na leitura do API').save()
            return
        self.valor.actual = moeda.actual
        self.valor.maximo = moeda.maximo
        self.valor.minimo = moeda.minimo
        self.valor.volume = moeda.volume

    def __ler_api(self):
        """Acede ao API e retorna os dados selecionados"""
        a = None
        try:
            data = request.urlopen(self.url_api, timeout=1).read().decode("utf8")
            a = loads(str(data))
        except KeyboardInterrupt:
            Log('\nlib_moedas: CTRL+C: A interronper leitura API').save()
        except timeout as timeout_error:
            Log('lib_moedas: urllib: Erro {}'.format(timeout_error)).save()
        except Exception as e:
            Log('lib_moedas: ERROR: Class Crypto: {}'.format(e)).save()

        actual = a["result"]["price"]["last"]
        maximo = a["result"]["price"]["high"]
        minimo = a["result"]["price"]["low"]
        volume = a['result']['volume']

        return Moeda(maximo, minimo, actual, volume)

    def __str__(self):
        return '{} ({})\n{}'.format(self.nome, self.simbolo, self.valor)


moedas = []


class BTC(Crypto):
    url_api = 'https://api.cryptowat.ch/markets/bitstamp/btceur/summary'
    nome = 'Bitcoin'
    simbolo = 'BTC'


moedas.append('BTC')


class LTC(Crypto):
    url_api = 'https://api.cryptowat.ch/markets/bitstamp/ltceur/summary'
    nome = 'Litecoin'
    simbolo = 'LTC'


moedas.append('LTC')


class XRP(Crypto):
    url_api = 'https://api.cryptowat.ch/markets/bitstamp/xrpeur/summary'
    nome = 'Ripple'
    simbolo = 'XRP'


moedas.append('XRP')


class ETH(Crypto):
    url_api = 'https://api.cryptowat.ch/markets/bitstamp/etheur/summary'
    nome = 'Ethereum'
    simbolo = 'ETH'


moedas.append('ETH')


class BCH(Crypto):
    url_api = 'https://api.cryptowat.ch/markets/bitstamp/bcheur/summary'
    nome = 'Bitcoin Cash'
    simbolo = 'BCH'


moedas.append('BCH')


def string_to_object(moeda=''):
    if moeda.upper() == 'BTC':
        return BTC()
    elif moeda.upper() == 'LTC':
        return LTC()
    elif moeda.upper() == 'XRP':
        return XRP()
    elif moeda.upper() == 'ETH':
        return ETH()
    elif moeda.upper() == 'BCH':
        return BCH()
    else:
        return None
