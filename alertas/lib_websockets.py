"""Powered by scdias@outlook.com
Módulo reponsável pela conecção ao websockets do BitStamp"""

from alertas.lib_install import install
from alertas.tbl_log import Log
from alertas.tbl_trades import Trades
from threading import Thread

try:
    from pysher import Pusher
except ImportError as e:
    print('\033[94m{}\033[0m'.format(e))
    install('pysher')  # TODO: Verificar se com a actualização do módulo ainda há problemas de connecção
    from pysher import Pusher

pusher = Pusher('de504dc5763aeef9ff52')


def save_btc(*args):
    from alertas.lib_utils import triger_alertas
    if args[0]:
        Thread(target=Trades(json_str=args[0], currency='BTC').save()).start()
        Thread(target=triger_alertas('BTC')).start()


def save_ltc(*args):
    from alertas.lib_utils import triger_alertas
    if args[0]:
        Thread(target=Trades(json_str=args[0], currency='LTC').save()).start()
        Thread(target=triger_alertas('LTC')).start()


def save_eth(*args):
    from alertas.lib_utils import triger_alertas
    if args[0]:
        Thread(target=Trades(json_str=args[0], currency='ETH').save()).start()
        Thread(target=triger_alertas('ETH')).start()


def save_xrp(*args):
    from alertas.lib_utils import triger_alertas
    if args[0]:
        Thread(target=Trades(json_str=args[0], currency='XRP').save()).start()
        Thread(target=triger_alertas('XRP')).start()


def save_bch(*args):
    from alertas.lib_utils import triger_alertas
    if args[0]:
        Thread(target=Trades(json_str=args[0], currency='BCH').save()).start()
        Thread(target=triger_alertas('BCH')).start()


def connect_handler(data):
    channel_btc = pusher.subscribe('live_trades_btceur')
    channel_btc.bind('trade', save_btc)

    channel_ltc = pusher.subscribe('live_trades_ltceur')
    channel_ltc.bind('trade', save_ltc)

    channel_xrp = pusher.subscribe('live_trades_xrpeur')
    channel_xrp.bind('trade', save_xrp)

    channel_eth = pusher.subscribe('live_trades_etheur')
    channel_eth.bind('trade', save_eth)

    channel_bch = pusher.subscribe('live_trades_bcheur')
    channel_bch.bind('trade', save_bch)

    Log('lib_websockets: Ligação efectuada: {}'.format(data)).save()


def connect_failed(data):
    Log('lib_websockets: Connection Failed: {}'.format(data)).save()


def connect_error(data):
    Log('lib_websockets: Pusher Error: {}'.format(data)).save()


def connect_ping(data):
    Log('lib_websockets: Pusher Ping: {}'.format(data)).save()


def connect_pong(data):
    Log('lib_websockets: Pusher Pong: {}'.format(data)).save()


def connect_websocket():
    pusher.connection.bind('pusher:connection_established', connect_handler)
    pusher.connection.bind('pusher:connection_failed', connect_failed)
    pusher.connection.bind('pusher:error', connect_error)
    pusher.connection.bind('pusher:ping', connect_ping)
    pusher.connection.bind('pusher:pong', connect_pong)

    pusher.connect()


def connection_state():
    return pusher.connection.state
