"""Powered by scdias@outlook.com
Tabela para guardar estatisticas de moedas"""

from time import time

from alertas.db_config import Base, Session, engine, CRUD
from alertas.lib_install import install
from alertas.lib_moedas import Moeda

try:
    from sqlalchemy import Column, Integer, String, REAL
except ImportError as e:
    print('\033[94m{}\033[0m'.format(e))
    install('sqlalchemy')
    from sqlalchemy import Column, Integer, String, REAL


class Stats(Base, CRUD):
    __tablename__ = 'stats'
    id = Column(Integer, primary_key=True)
    datahora = Column(Integer)
    moeda = Column(String)
    maximo = Column(REAL)
    minimo = Column(REAL)
    actual = Column(REAL)
    volume = Column(REAL)

    def __init__(self, moeda=None, valor=Moeda()):
        self.moeda = moeda
        self.maximo = valor.maximo
        self.minimo = valor.minimo
        self.actual = valor.actual
        self.volume = valor.volume
        self.datahora = time()

    def save(self):
        if self.actual != 0:
            local_session = Session()
            local_session.add(self)
            local_session.commit()
            # Session.remove()


Base.metadata.create_all(engine)
