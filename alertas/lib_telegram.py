"""Powered by scdias@outlook.com
Lib Telegram"""
from threading import Thread
from time import sleep

from alertas.lib_install import install
from alertas.tbl_log import Log
from alertas.lib_moedas import BTC, LTC, XRP, ETH, BCH

try:
    from telegram import Bot, InlineKeyboardButton, InlineKeyboardMarkup
except ImportError as e:
    print('\033[94m{}\033[0m'.format(e))
    install('python-telegram-bot')
    from telegram import Bot, InlineKeyboardButton, InlineKeyboardMarkup

try:
    from signalslot import Signal
except ImportError as e:
    print('\033[94m{}\033[0m'.format(e))
    install('signalslot')
    from signalslot import Signal

from telegram.ext import Updater, MessageHandler, Filters, CommandHandler, CallbackQueryHandler
from alertas.definitions import token_Telegram, admin_ids

updater = Updater(token=token_Telegram)
dispatcher = updater.dispatcher


def envia_msg(user_id, msg):
    """Envia msg a um utilizador genérico"""
    bot = Bot(token=token_Telegram)
    try:
        bot.sendMessage(chat_id=user_id, text=msg)
    except KeyboardInterrupt:
        Log('\nlib_telegram: CTRL+C: A cancelar envio de mensagem....').save()
        signal_exit_telegram.emit()
    except Exception as e_envia_msg:
        Log('lib_telegram: Erro a enviar mensagem: {}.\nA tentar de novo em 30 segundos.'.format(e_envia_msg)).save()
        sleep(30)
        envia_msg(user_id, msg)


def envia_admin_msg(msg):
    """Envia Mensagem aos admin_ids"""
    for admin_id in admin_ids:
        envia_msg(user_id=admin_id, msg=msg)


signal_exit_telegram = Signal()


def termina_bot():
    """Tarefas para encerramento do bot"""
    global signal_exit_telegram
    signal_exit_telegram.emit()


def acesso_permitido(bot, update):
    """Verifica se o comando tem como origem a conta de administrador do Bot"""
    user_id = update.message.from_user.id
    user = update.message.from_user
    msg = update.message.text
    Log('lib_telegram: {}\n{}'.format(user, msg)).save()
    if user_id not in admin_ids:
        envia_msg(user_id, 'Private Bot. Your action have been reported to admin')
        admin_msg = ('Mensagem não identificada.\n'
                     'User: {}\n'
                     'Mensagem: {}'.format(user, msg))
        envia_admin_msg(admin_msg)
        return False
    else:
        return True


'''---------------------------------SLOTS-----------------------------------'''


def slot_envia_admin_msg(msg, **kwargs):
    try:
        Thread(target=envia_admin_msg, args=(msg,), daemon=True).start()
    except KeyboardInterrupt:
        Log('\nlib_telegram: CTRL+C: A cancelar envio de mensagem.').save()
        signal_exit_telegram.emit()
    except Exception as e_slot_envia_admin_msg:
        Log('lib_telegram: Erro ao iniciar thread de envio de mensagem: {}'.format(e_slot_envia_admin_msg)).save()


'''-----------------------------COMANDOS DO BOT-----------------------------'''


def start(bot, update):
    """/start"""
    if not acesso_permitido(bot, update):
        return
    envia_msg(update.message.chat_id, "I'm a bot, please talk to me!")


start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)


def stop(bot, update):
    """/stop"""
    if not acesso_permitido(bot, update):
        return
    termina_bot()


stop_handler = CommandHandler('stop', stop)
dispatcher.add_handler(stop_handler)


def difbtc(bot, update):
    """/difbtc"""
    if not acesso_permitido(bot, update):
        return

    from alertas.lib_telegram_cmd import difbtc_cmd
    Thread(target=difbtc_cmd(user_id=update.message.chat_id)).start()


difbtc_handler = CommandHandler('difbtc', difbtc)
dispatcher.add_handler(difbtc_handler)


def valor(bot, update):
    """/valor"""
    if not acesso_permitido(bot, update):
        return

    keyboard = [[InlineKeyboardButton("BTC", callback_data='BTC'),
                 InlineKeyboardButton("LTC", callback_data='LTC'),
                 InlineKeyboardButton("XRP", callback_data='XRP'),
                 InlineKeyboardButton("ETH", callback_data='ETH'),
                 InlineKeyboardButton("BCH", callback_data='BCH'),
                 ]]

    reply_markup = InlineKeyboardMarkup(keyboard)

    update.message.reply_text('Escolha a moeda:', reply_markup=reply_markup)


valor_handler = CommandHandler('valor', valor)
dispatcher.add_handler(valor_handler)


def button(bot, update):
    query = update.callback_query
    msg = ''
    if query.data == 'BTC':
        msg = format(BTC())
    elif query.data == 'LTC':
        msg = format(LTC())
    elif query.data == 'XRP':
        msg = format(XRP())
    elif query.data == 'ETH':
        msg = format(ETH())
    elif query.data == 'BCH':
        msg = format(BCH())

    bot.edit_message_text(text=msg,
                          chat_id=query.message.chat_id,
                          message_id=query.message.message_id)


button_handler = CallbackQueryHandler(button)
dispatcher.add_handler(button_handler)


def alerta(bot, update, args):
    """/alerta"""
    if not acesso_permitido(bot, update):
        return

    # Verificar se não tem parametros
    if len(args) == 0:
        from alertas.lib_telegram_cmd import list_alertas
        Thread(target=list_alertas(update.message.chat_id)).start()
        return

    # Verificar número de parametros
    if len(args) != 2:
        envia_msg(update.message.chat_id, 'Número incorreto de parâmetros.\n'
                                          '/alerta <moeda> <valor>')
        return

    # Verificar se string é moeda suportada
    from alertas.lib_moedas import moedas
    if args[0].upper() not in moedas:
        envia_msg(update.message.chat_id, 'Moeda {} não suportada'.format(args[0]))
        return

    # Verificar se 2º parametro é número
    try:
        _valor = float(args[1])
    except ValueError:
        envia_msg(update.message.chat_id, 'Formato incorreto nos parâmetros.\n'
                                          '/alerta <moeda> <valor>')

    from alertas.lib_telegram_cmd import alerta_cmd
    Thread(target=alerta_cmd(moeda=args[0], valor=args[1], user_id=update.message.chat_id)).start()


alerta_handler = CommandHandler('alerta', alerta, pass_args=True)
dispatcher.add_handler(alerta_handler)


def grafico(bot, update):
    """/grafico"""
    if not acesso_permitido(bot, update):
        return
    from telegram import ChatAction
    from alertas.lib_telegram_cmd import grafico_cmd

    bot.send_chat_action(chat_id=update.message.chat_id, action=ChatAction.UPLOAD_PHOTO)
    bot.send_photo(chat_id=update.message.chat_id, photo=open(grafico_cmd(), 'rb'))


grafico_handler = CommandHandler('grafico', grafico)
dispatcher.add_handler(grafico_handler)


def cmd_help(bot, update):
    """/help"""
    if not acesso_permitido(bot, update):
        return
    msg = (
        u'\u2753\n'
        u'\U0001F449 /alerta <moeda> <valor>: Cria um alerta para o valor selecionado\n'
        u'\U0001F449 /difbtc: Retorna a diferença entre os máximos e mínimos das útimas 24horas de BTC\n'
        u'\U0001F449 /grafico: Gera gráfico de valor/volume das últimas 24hs\n'
        u'\U0001F449 /help: Mostra esta ajuda\n'
        u'\U0001F449 /start: Inicia o bot\n'
        u'\U0001F449 /stop: Encerra o bot\n'
        u'\U0001F449 /valor: Retorna os valores da moeda selecionada\n'

    )
    envia_msg(update.message.chat_id, msg)


help_handler = CommandHandler('help', cmd_help)
dispatcher.add_handler(help_handler)


def unknown(bot, update):
    """Funcao executada quando o comando não é reconhecido
    É recomendado esta função ser a última a ser implementada"""
    if not acesso_permitido(bot, update):
        return
    envia_admin_msg('Not yet implemented!!!!')


unknown_handler = MessageHandler(Filters.all, unknown)
dispatcher.add_handler(unknown_handler)

'''--------------------------INICIALIZAÇÂO DO BOT---------------------------'''


def init_bot():
    envia_admin_msg(u'\U0001F916 Iniciado bot')
    try:
        Thread(target=updater.start_polling, daemon=True).start()
    except KeyboardInterrupt:
        Log('\nlib_telegram: CTRL+C: A cancelar início do bot.').save()
        signal_exit_telegram.emit()
    except Exception as e_init_bot:
        Log('lib_telegram: Erro init_bot: {}'.format(e_init_bot)).save()


try:
    init_bot()
except (KeyboardInterrupt, SystemExit):
    Log('\nlib_telegram: Received keyboard interrupt, quitting threads.\n').save()
    signal_exit_telegram.emit()
except Exception as e:
    Log('lib_telegram: Erro na inicialização do bot: {}.\nA tentar denovo em 30 segundos.'.format(e)).save()
    sleep(30)
    init_bot()
