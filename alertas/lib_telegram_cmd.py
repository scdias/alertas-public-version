"""Powered by scdias@outlook.com
Implementação dos comandos publicos do telegram"""

from alertas.lib_install import install

try:
    from matplotlib import use
    use('Agg')
except ImportError as e:
    print('\033[94m{}\033[0m'.format(e))
    install('matplotlib')
    from matplotlib import use
    use('Agg')

from matplotlib import pyplot


def alerta_cmd(moeda='', valor=0.0, user_id=''):
    from alertas.lib_moedas import string_to_object
    from alertas.tbl_alertas import Alertas
    from alertas.lib_telegram import envia_msg
    valor = float(valor)
    moeda_obj = string_to_object(moeda)
    actual = moeda_obj.valor.actual
    if valor < actual:
        sinal = '<'
    elif valor > actual:
        sinal = '>'
    else:
        return

    if num_alertas(moeda=moeda.upper(), valor=valor, sinal=sinal) != 0:
        envia_msg(user_id, u'\u26D4 Já existe o alerta selecionado.')
        return

    alerta = Alertas(moeda=moeda.upper(), valor=valor, sinal=sinal)
    alerta.save()
    envia_msg(user_id, u'\U0001F4BD Alerta gravado:\n{}'.format(alerta))


def difbtc_cmd(user_id=''):
    from alertas.lib_moedas import BTC

    btc = BTC()
    if btc == 0:
        msg = 'Erro de leitura de API'
    else:
        msg = 'Diferença de extremos em BTC: {}.'.format(round(btc.valor.maximo - btc.valor.minimo, 2))
    from alertas.lib_telegram import envia_msg
    envia_msg(user_id=user_id, msg=msg)


def grafico_cmd():  # TODO: Verificar a possibilidade de mudar para a tabela trades
    from alertas.db_config import Session
    from alertas.tbl_stats import Stats
    from time import time

    local_session = Session()

    data = local_session \
        .query(Stats.datahora, Stats.actual, Stats.volume) \
        .filter(Stats.moeda == 'BTC') \
        .filter(Stats.datahora > time() - 86400) \
        .order_by(Stats.datahora) \
        .all()

    tempo, valor, volume = zip(*data)

    pyplot.figure()
    pyplot.suptitle('Volume / Preço BTC 24h')
    pyplot.subplot(211)
    pyplot.plot(tempo, valor, 'r', label='Preço')
    # pyplot.title('Valor de BTC - 24h')
    # pyplot.xlabel('Timestamp')
    # TODO: Verificar eliminação do eixo XX
    # pyplot.axes.get_xaxis().set_visible(False)
    pyplot.xticks([])
    # TODO:--------------------------------
    pyplot.ylabel('Preço')
    pyplot.legend()
    pyplot.grid()

    pyplot.subplot(212)
    pyplot.plot(tempo, volume, 'b', label='Volume')
    # pyplot.title('Volume de BTC - 24h')
    pyplot.xlabel('TimeStamp')
    pyplot.ylabel('Volume 24h')
    pyplot.legend()
    pyplot.grid()

    filename = '/tmp/Alertas_{}.png'.format(time())
    pyplot.savefig(filename)
    pyplot.close('all')
    # Session.remove()
    return filename


def num_alertas(moeda='', sinal='', valor=0.0):
    from alertas.tbl_alertas import Alertas
    from alertas.db_config import Session

    local_session = Session()

    numero_alertas = local_session \
        .query(Alertas) \
        .filter(Alertas.moeda == moeda) \
        .filter(Alertas.sinal == sinal) \
        .filter(Alertas.valor == valor) \
        .all()

    return len(numero_alertas)


def list_alertas(user_id=''):
    from alertas.tbl_alertas import Alertas
    from alertas.db_config import Session
    from alertas.lib_telegram import envia_msg

    local_session = Session()

    lista = local_session \
        .query(Alertas) \
        .order_by(Alertas.moeda) \
        .order_by(Alertas.valor) \
        .all()

    if len(lista) == 0:
        envia_msg(user_id=user_id, msg=u'\u23F0 Sem alertas registados.')
        return

    msg = u'\u23F0 Alertas registados:\n'

    for u in lista:
        msg = '{}{}\n'.format(msg, u)

    envia_msg(user_id=user_id, msg=msg)
