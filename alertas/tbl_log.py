"""Powered by scdias@outlook.com
Tabela de Log de eventos"""

from alertas.db_config import Base, engine, CRUD, Session
from alertas.lib_install import install

try:
    from sqlalchemy import Column, Integer, String
except ImportError as e:
    print('\033[94m{}\033[0m'.format(e))
    install('sqlalchemy')
    from sqlalchemy import Column, Integer, String


class Log(Base, CRUD):
    __tablename__ = 'logs'
    id = Column(Integer, primary_key=True)
    datahora = Column(Integer)
    datahora_str = Column(String)
    texto = Column(String)

    def __init__(self, log) -> None:
        self.texto = log
        from time import time, ctime
        self.datahora = time()
        self.datahora_str = ctime(self.datahora)

    def save(self):
        local_session = Session()
        local_session.add(self)
        local_session.commit()
        print('\033[92m{}\033[0m'.format(self))
        # Session.remove()

    def __str__(self):
        return '{}: {}'.format(self.datahora_str, self.texto)


Base.metadata.create_all(engine)
